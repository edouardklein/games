;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages baba-is-you)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (games humble-bundle))

;; TODO: Should baba-is-you be a symbol?  ISO-based setups like Quake 3 may need
;; to support spaces.

(define-public baba-is-you
  (let ((system (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "bin64")
                  ("i686-linux"   "bin32"))))
    (package
      (name "baba-is-you")
      (version "1.0.4")
      (source (origin
               (method humble-bundle-fetch)
               (uri (humble-bundle-reference
                      (help (humble-bundle-help-message name))
                      (config-path '(baba-is-you key))))
               (file-name "BIY_linux_20mar17.tar.gz")
               (sha256
                (base32
                 "1jbmfdqnlgmzvrcs0pq754kzjxgz2s3jkbfpwvzb97d0j11432bk"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append system "/Chowdren")
            ("dbus" "eudev" "libc" "libxcursor" "libxi" "libxinerama" "libxrandr"
             "libxscrnsaver" "mesa" "pulseaudio")))
         #:install-plan
         `((,,system ("Chowdren") "share/baba-is-you/")
           ("." ("gamecontrollerdb.txt" "Assets.dat") "share/baba-is-you/")
           ("Data" (".*") "share/baba-is-you/Data/")
           ("bin" ("baba-is-you") "bin/"))
         #:phases
         (modify-phases %standard-phases
           (add-before 'install 'create-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (mkdir "bin")
               (let ((baba-wrapper "bin/baba-is-you"))
                 (with-output-to-file baba-wrapper
                   (lambda _
                     (format #t "#!~a~%" (which "bash"))
                     (format #t "cd ~a~%" (string-append (assoc-ref outputs "out")
                                                         "/share/baba-is-you"))
                     (format #t "./Chowdren")))
                 (chmod baba-wrapper #o755))
               #t)))))
      (inputs
       `(("dbus" ,dbus)
         ("eudev" ,eudev)
         ("libxcursor" ,libxcursor)
         ("libxinerama" ,libxinerama)
         ("libxi" ,libxi)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)))
      (home-page "http://www.hempuli.com/baba/")
      (synopsis "Puzzle game where you can change the rules")
      (description "Baba Is You is a puzzle game where the rules you have to
follow are present as physical objects in the game world.  By manipulating the
rules, you can change how the game works, repurpose things you find in the
levels and cause surprising interactions!")
      (license (undistributable "No URL")))))

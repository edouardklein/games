;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages lyne)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public lyne
  (let ((binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "LYNE.x86_64")
                  (_              "LYNE.x86"))))
    (package
      (name "lyne")
      (version "132")
      (source
       (origin
        (method game-local-fetch)
        (uri "LYNE_132_lin.tgz")
        (sha256
         (base32
          "181nslcjdyz5ngx42y9xcp6vgi2awjb7b6ind4w6dyiynmd1naqw"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system binary-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "gcc:lib" "eudev" "gdk-pixbuf" "glib" "glu" "gtk+-2" "libx11"
             "libxcursor" "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
             "libxxf86vm" "mesa" "pulseaudio" "zlib"))
           ("LYNE_Data/Plugins/x86_64/ScreenSelector.so"
            ("libc" "gcc:lib" "gtk+-2" "gdk-pixbuf" "glib"))
           ("LYNE_Data/Plugins/x86/ScreenSelector.so"
            ("libc" "gcc:lib" "gtk+-2" "gdk-pixbuf" "glib")))
         #:install-plan
         `((,,binary "share/lyne/")
           ("LYNE_Data" "share/lyne/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'make-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (wrapper (string-append out "/bin/lyne"))
                      (binary (string-append out "/share/lyne/" ,binary)))
                 (make-wrapper
                   wrapper binary
                   #:skip-argument-0? #t
                   `("PATH" ":" prefix
                     (,(string-append (assoc-ref inputs "pulseaudio") "/bin")))))
               #t))
           (add-after 'install 'make-desktop-entry
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (make-desktop-entry-file
                   (string-append out "/share/applications/lyne.desktop")
                   #:name "Lyne"
                   #:icon (assoc-ref inputs "icon")
                   #:exec (string-append out "/bin/lyne")
                   #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("glu" ,glu)
         ("gtk+-2" ,gtk+-2)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxext" ,libxext)
         ("libxi" ,libxi)
         ("libxinerama" ,libxinerama)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("libxxf86vm" ,libxxf86vm)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("zlib" ,zlib)))
      (native-inputs
       `(("icon" ,(origin
                    (method url-fetch)
                    (uri "https://img.itch.zone/aW1hZ2UvNjc0LzMyMjcucG5n/original/XJl9Us.png")
                    (sha256
                     (base32
                      "1246hkl3pr5715xbrcv7hyiqbr4s7ycm31wbx6rs0d07fr19pr0l"))))))
      (home-page "https://thomasbowker.itch.io/lyne")
      (synopsis "Minimalist puzzle game")
      (description "Deceptively simple.  Infinitely complex.  LYNE is a
minimalist puzzle game that will knot your brain as it calms your soul.

Connect the shapes.  Fill the board.  Lose yourself in the interflowing paths
of LYNE.")
      (license (undistributable "No License")))))
